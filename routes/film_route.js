const router = require('express').Router();
const FilmController = require('../controllers/film_controller');

router.route('/')
    .get(FilmController.getAll)
    .post(FilmController.create);

router.route('/:id')
    .get(FilmController.getOne)
    .put(FilmController.updateOne)
    .delete(FilmController.deleteOne);

module.exports = router;
