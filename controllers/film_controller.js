const connection = require('../db/connect');

function getAll(req, res) {
    connection.query('SELECT * from film', function (error, results) {
        if (error) {
            return res.status(400).json({success: false, error: error.message});
        }
        if (!results.length) {
            return res.status(404).json({success: false, error: "No films found."});
        }
        return res.status(200).json({success: true, data: results});
    });
}

function create(req, res) {
    const body = req.body;
    if (!Object.keys(body).length) {
        return res.status(400).json({ success: false, error: "You must specify a film." });
    }

    connection.query('INSERT INTO film SET ?', body, function (error, result) {
        if (error) {
            return res.status(400).json({success: false, error: error.message});
        }
        return res.status(200).json({success: true, id: result.insertId, message: "Film created."});
    });
}

function getOne(req, res) {
    connection.query('SELECT * FROM film WHERE film_id = ?', [req.params.id], function (error, results) {
        if (error) {
            return res.status(400).json({success: false, error: error.message});
        }
        if (!results.length) {
            return res.status(404).json({success: false, error: "No films found."});
        }
        return res.status(200).json({success: true, data: results[0]});
    });
}


function updateOne(req, res) {
    const body = req.body;
    if (!Object.keys(body).length) {
        return res.status(400).json({ success: false, error: "You must provide some data to update."});
    }

    connection.query('UPDATE film SET ? WHERE film_id = ?', [body, req.params.id], function (error, result) {
        if (error) {
            return res.status(400).json({success: false, error: error.message});
        }
        if (!result.affectedRows) {
            return res.status(404).json({success: false, error: "No film found."});
        }
        return res.status(200).json({success: true, message: "Film updated."});
    });
}


function deleteOne(req, res) {
    connection.query("DELETE FROM film WHERE film_id = ?", [req.params.id], function (error, results) {
        if (error) {
            return res.status(400).json({success: false, error: error.message});
        }
        if (!results.affectedRows) {
            return res.status(404).json({success: false, error: "No film found."});
        }
        return res.status(200).json({success: true, message: "Film deleted."});
    })
}

module.exports = {getAll, create, getOne, updateOne, deleteOne};