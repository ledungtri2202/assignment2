const connection = require('./db/connect');
const express = require('express');
const filmRoute = require('./routes/film_route');

const backend = express();
backend.use(express.json());
backend.use('/api/films', filmRoute);

backend.listen(3001, function () {
    console.log("Server started successfully on port 3001...")
});